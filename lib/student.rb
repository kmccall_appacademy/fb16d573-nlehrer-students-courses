class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    if @courses.any? { |old_course| old_course.conflicts_with?course }
      raise 'CONFLICT!!!!!?!?'
    else
      @courses << course unless @courses.include?(course)
      course.students << self
    end
  end

  def course_load
    crs_load_hash = Hash.new(0)
    @courses.each { |crs| crs_load_hash[crs.department] += crs.credits }
    crs_load_hash
  end
end
